﻿using System;
using static System.Console;

char @continue;

do
{
    int borderValue;
    while (true)
    {
        Write("Enter border value > 0: ");
        borderValue = int.Parse(ReadLine()!);
        Clear();
        if (borderValue > 0)
        {
            break;
        }

        WriteLine("Invalid value. Try again.");
    }

    int count;
    while (true)
    {
        Write("Enter count value > 0: ");
        count = int.Parse(ReadLine()!);
        Clear();
        if (count > 0)
        {
            break;
        }

        WriteLine("Invalid value. Try again.");
    }

    const string fizz = "Fizz";
    const string buzz = "Buzz";

    for (int i = 1; i <= borderValue; i++)
    {
        string value = (i % 3) switch
        {
            0 when i % 5 == 0 => "FizzBuzz", //$"{fizz}{buzz}",
            0 => fizz, //"Fizz"
            _ => i % 5 == 0 ? buzz : $"{i}"
        };

        string separator = (i % count) switch
        {
            0 when i == borderValue => string.Empty, //""
            0 => $",{Environment.NewLine}",
            //_ when i == borderValue => string.Empty,
            _ => i == borderValue ? string.Empty : ", ",
        };

        Write($"{value}{separator}");
    }

    WriteLine();
    WriteLine("Press 'y' or 'Y' if want to continue.");
    @continue = char.Parse(ReadLine()!);
    Clear();
} while (@continue is 'y' or 'Y');

WriteLine("Finished!");
