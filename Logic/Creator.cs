﻿using System;
using System.Dynamic;

public static class Creator
{
    public static string Create(int borderValue, int count, string firstWord, string secondWord, int a, int b)
    {
        string result = string.Empty;
        
        for (int i = 1; i <= borderValue; i++)
        {
            string value = (i % a) switch
            {
                0 when i % b == 0 => $"{firstWord}{secondWord}",
                0 => firstWord,
                _ => i % b == 0 ? secondWord : $"{i}"
            };

            string separator = (i % count) switch
            {
                0 when i == borderValue => string.Empty,
                0 => $",{Environment.NewLine}",
                _ => i == borderValue ? string.Empty : ", ",
            };

            result = result + $"{value}{separator}";
        }

        return result;
    }
}