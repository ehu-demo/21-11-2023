﻿using System;
using static System.Console;

char @continue;

do
{
    int borderValue;
    while (true)
    {
        Write("Enter border value > 0: ");
        borderValue = int.Parse(ReadLine()!);
        Clear();
        if (borderValue > 0)
        {
            break;
        }

        WriteLine("Invalid value. Try again.");
    }

    int count;
    while (true)
    {
        Write("Enter count value > 0: ");
        count = int.Parse(ReadLine()!);
        Clear();
        if (count > 0)
        {
            break;
        }

        WriteLine("Invalid value. Try again.");
    }

    const string fizz = "Fizz";
    const string buzz = "Buzz";

    string result = Creator.Create(borderValue, count, "One", "Two", 4, 6);
    
    Write(result);

    WriteLine();
    WriteLine("Press 'y' or 'Y' if want to continue.");
    @continue = char.Parse(ReadLine()!);
    Clear();
} while (@continue is 'y' or 'Y');

WriteLine("Finished!");